<?php

use PartsSearch\Search;

define('ENV_PATH', __DIR__);

require __DIR__ . '/vendor/autoload.php';

$search = new Search;
$search->getResponse();
