<?php

namespace PartsSearch\Interfaces;

interface ShouldRespond
{
    /**
     * Return response from API
     *
     * @return mixed
     */
    public function getResponse();

    /**
     * Get request's query
     *
     * @return array
     */
    public function getQuery();

    /**
     * Set request's query
     *
     * @return void
     */
    public function setQuery();

    /**
     * List of search results
     *
     * @param  array  $response
     *
     * @return array
     */
    public function collection(array $response);
}
