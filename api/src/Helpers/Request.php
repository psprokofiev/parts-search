<?php

namespace PartsSearch\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use PartsSearch\Search;

class Request
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $query = [];

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var array
     */
    private $auth = [];

    /**
     * Request constructor.
     *
     * @param  string  $url
     */
    public function __construct(string $url)
    {
        $this->setUrl($url);
    }

    /**
     * Return request body
     *
     * @param  string  $method
     *
     * @return string
     */
    public function getResponse($method = 'GET')
    {
        $response = null;

        try {
            $client   = new Client();
            $response = $client->request($method, $this->url, $this->getOptions());
        } catch (RequestException $e) {
            $caught = Psr7\str($e->getRequest());
            Search::log($caught);

            if ($e->hasResponse()) {
                $response = Psr7\str($e->getResponse());
                Search::log($response);
            }

            Response::error($caught . PHP_EOL . $response);
        }

        $code = $response->getStatusCode();
        if ($code > 200) {
            // if request has invalid response
            Response::error('Invalid response', $code);
        }

        return (string) $response->getBody()->getContents();
    }

    /**
     * Request's options array
     *
     * @return array
     */
    private function getOptions()
    {
        return [
            'headers' => $this->headers,
            'query'   => $this->query,
            'auth'    => $this->auth,
        ];
    }

    /**
     * @param  array  $query
     *
     * @return Request
     */
    public function setQuery(array $query): Request
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @param  array  $headers
     *
     * @return Request
     */
    public function setHeaders(array $headers): Request
    {
        $this->headers               = $headers;
        $this->headers['User-Agent'] = 'Moliza/5.0';

        return $this;
    }

    /**
     * @param  array  $auth
     *
     * @return Request
     */
    public function setAuth(array $auth): Request
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * @param  string  $url
     */
    private function setUrl($url): void
    {
        $this->url = $url;
    }
}
